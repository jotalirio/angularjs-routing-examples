angular
    .module('ordersapp')
    .controller('OrdersCtrl', function($scope, OrdersService){
        $scope.orders = OrdersService.getOrders();
    });